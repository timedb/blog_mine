package model

import (
	"blog_service/setting"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var Db, _ = gorm.Open("mysql", setting.Mysql)

type Type struct {
	ID       uint   `gorm:"primary_key"`
	TypeName string `gorm:"size:255;not null;unique"`
	OrderNum int
	Icon     string `gorm:"size:20"`
}

type Article struct {
	ID             uint   `gorm:"primary_key"`
	TypeId         uint   `gorm:"not null"`
	Title          string `gorm:"size:255;not null"`
	ArticleContent string `gorm:"type:text;not null"`
	Introduce      string `gorm:"size:255"`
	AddTime        int
	ViewCount      int
}

type AdminUser struct {
	ID       uint   `gorm:"primary_key"`
	Username string `gorm:"size:255;not null"`
	Password string `gorm:"size:255;not null"`
}

func init() {
	db, err := gorm.Open("mysql", setting.Mysql)
	if err != nil {
		panic(err)
	}

	defer db.Close()

	db.AutoMigrate(&Type{}, &Article{}, &AdminUser{})

}
