module blog_service

go 1.15

require (
	github.com/garyburd/redigo v1.6.2
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.16
	github.com/robfig/cron v1.2.0 // indirect
)
