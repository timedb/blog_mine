package admin

import (
	"github.com/garyburd/redigo/redis"
	"github.com/gin-gonic/gin"
)

func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Query("token")
		user := c.Query("user")
		if token == "" || user == "" {
			c.JSON(401, gin.H{"Data": "server err"})
			c.Abort()
			return
		}

		conn, err := CreateRedisConn()
		if err != nil {
			c.JSON(500, gin.H{"Data": "server err"})
			c.Abort()
			return
		}
		defer conn.Close()

		tokenRedis, err := redis.String(conn.Do("GET", user))
		if err != nil {
			c.JSON(401, gin.H{"Data": "verify err"})
			c.Abort()
			return
		}

		if token != tokenRedis {
			c.JSON(401, gin.H{"Data": "server err"})
			c.Abort()
			return
		}

		c.Next()

	}
}
