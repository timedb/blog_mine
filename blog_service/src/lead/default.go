package lead

import (
	"blog_service/model"
	"blog_service/src/admin"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"github.com/gin-gonic/gin"
	"regexp"
)

func Index(c *gin.Context) {
	c.String(200, "awdawd")
}

type ErrJson map[string]string

type ArticleList struct {
	model.Article
	TypeTitle string
	AddTimes  string `json:"AddTime"`
}

func ReadComment(id string, update bool) int {
	//观看数
	conn, err := admin.CreateRedisConn()

	if err == nil {
		defer conn.Close()
		comment, err := redis.Int(conn.Do("get", fmt.Sprintf("comment_%s", id)))
		if err != nil {
			conn.Do("set", fmt.Sprintf("comment_%s", id), 0)
			return 0
		} else {
			if update { //跟新
				conn.Do("set", fmt.Sprintf("comment_%s", id), comment+1)
				return comment + 1
			} else { //读取
				return comment
			}

		}

	} else {
		return 0
	}
}

func GetArticleList(c *gin.Context) {

	articleList := make([]ArticleList, 1)

	model.Db.Table("articles").
		Select(
			"articles.id as id," +
				"articles.title as title," +
				"articles.introduce as introduce," +
				"FROM_UNIXTIME(articles.add_time, '%Y-%m-%d %H:%i:%s') as add_times," +
				"articles.view_count as view_count," +
				"types.type_name as type_title").
		Joins("left join types on articles.type_id = types.id").Order("id desc").Scan(&articleList)

	for index, item := range articleList {
		comment := ReadComment(fmt.Sprintf("%d", item.ID), false) //跟新阅读数
		if item.ViewCount < comment {
			articleList[index].ViewCount = comment
		}
	}

	c.JSON(200, map[string][]ArticleList{"Data": articleList})

}

func GetArticleById(c *gin.Context) {

	id := c.DefaultQuery("id", "")
	if id == "" {
		c.JSON(400, ErrJson{"err": "must have id"})
		return
	}

	if m, _ := regexp.MatchString(`^\d+$`, id); !m {
		c.JSON(400, ErrJson{"err": "id err"})
		return
	}

	article := new(ArticleList)

	model.Db.Table("articles").
		Select(
			"articles.id as id,"+
				"articles.title as title,"+
				"articles.article_content as article_content,"+
				"articles.introduce as introduce,"+
				"FROM_UNIXTIME(articles.add_time, '%Y-%m-%d %H:%i:%s') as add_times,"+
				"articles.view_count as view_count,"+
				"types.type_name as type_title").
		Joins("left join types on articles.type_id = types.id").Where("articles.id = ?", id).Scan(&article)

	comment := ReadComment(id, true) //跟新阅读数
	if article.ViewCount < comment {
		article.ViewCount = comment
	}

	if article.ID == 0 {
		c.JSON(200, ErrJson{"Data": ""})
		return
	}

	c.JSON(200, map[string]*ArticleList{"Data": article})
}

func GetTypeInfo(c *gin.Context) {
	types := make([]model.Type, 1)

	model.Db.Find(&types)

	c.JSON(200, map[string][]model.Type{"Data": types})

}

func GetListById(c *gin.Context) {

	id := c.DefaultQuery("id", "")

	if id == "" {
		c.JSON(400, ErrJson{"err": "not id"})
		return
	}

	if m, _ := regexp.MatchString(`^\d+$`, id); !m {
		c.JSON(400, ErrJson{"err": "id err"})
		return
	}

	articleList := make([]ArticleList, 1)

	model.Db.Table("articles").
		Select(
			"articles.id as id,"+
				"articles.title as title,"+
				"articles.introduce as introduce,"+
				"FROM_UNIXTIME(articles.add_time, '%Y-%m-%d %H:%i:%s') as add_times,"+
				"articles.view_count as view_count,"+
				"types.type_name as type_title").
		Joins("left join types on articles.type_id = types.id").
		Where("types.id = ?", id).Scan(&articleList)

	c.JSON(200, map[string]*[]ArticleList{"Data": &articleList})
}
