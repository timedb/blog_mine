import React from 'react';
import Head from 'next/head';
import Axios from "axios";

import {Row, Col, Breadcrumb, Affix} from 'antd';

import {CalendarOutlined, ReadOutlined, FireFilled} from "@ant-design/icons";

import Header from '../components/Header';
import Author from '../components/Author';
import Footer from "../components/Footer";
import Advert from '../components/Advert';

//Markdown
import MarkNav from 'markdown-navbar';
import marked from 'marked';
import hljs from 'highlight.js'
import Tocify from "../components/tocify.tsx";

import "highlight.js/styles/monokai-sublime.css";
import 'markdown-navbar/dist/navbar.css';
import "../static/style/pages/detailed.css"

//url
import servicePath from "../config/apiUrl";


const Detailed = (props) => {

    let article = props.Data

    const renderer = new marked.Renderer()

    const tocify = new Tocify()
    renderer.heading = function (text, level, raw) {
        const anchor = tocify.add(text, level);
        return `<a id="${anchor}" href="#${anchor}" class="anchor-fix"><h${level}>${text}</h${level}></a>\n`;
    };

    marked.setOptions(
        {
            renderer: renderer,
            gfm: true,
            pedantic: false,
            sanitize: false,
            tables: true,
            breaks: false,
            smartLists: true,
            highlight: function (code) {
                return hljs.highlightAuto(code).value;
            }
        }
    )

    let html = marked(article.ArticleContent);


    return (
        <div>
            <Head>
                <title>Detailed</title>
            </Head>
            <Header/>
            <Row className="comm-main" type="flex" justify="center">
                <Col className="comm-left" xs={24} sm={24} md={16} lg={18} xl={14}>
                    <div className="bread-div">
                        <Breadcrumb>
                            <Breadcrumb.Item><a href="/">首页</a> </Breadcrumb.Item>
                            <Breadcrumb.Item><a href="/">文章列表</a> </Breadcrumb.Item>
                            <Breadcrumb.Item>{article.Title}</Breadcrumb.Item>
                        </Breadcrumb>
                    </div>

                    <div className="detailed-title">
                        文章Title
                    </div>
                    <div className="list-icon">
                        <span><CalendarOutlined/>{article.AddTime}</span>
                        <span><ReadOutlined/>{article.TypeTitle}</span>
                        <span><FireFilled/>{article.ViewCount}</span>
                    </div>

                    <div className="detailed-content"
                         dangerouslySetInnerHTML={{__html: html}}>
                    </div>

                </Col>

                <Col className="comm-right" xs={0} sm={0} md={7} lg={5} xl={4}>
                    <Author/>
                    <Advert/>
                    <Affix offsetTop={5}>
                        <div className="detailed-nav comm-box">
                            <div className="nav-title">文章目录</div>
                            <div className="toc-list">
                                {tocify && tocify.render()}
                            </div>
                        </div>
                    </Affix>
                </Col>
            </Row>
            <Footer/>

        </div>
    )
}

Detailed.getInitialProps = async (context) => {
    let id = context.query.id;
    const promise = new Promise((resolve => {
        Axios(servicePath.getArticleById + id).then(
            (res) => {
                resolve(res.data);
            }
        )
    }))


    return await promise
}

export default Detailed