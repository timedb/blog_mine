import Head from 'next/head';
import {Row, Col, List, Breadcrumb} from 'antd';
import Header from '../components/Header';
import React, {useState, useEffect} from 'react';
import {ClockCircleTwoTone, ContainerTwoTone, FireTwoTone} from "@ant-design/icons";
import Author from '../components/Author'
import Advert from '../components/Advert'
import Footer from "../components/Footer";

import Axios from "axios";
import servicePath from "../config/apiUrl";
import Link from "next/link";

import marked from "marked";
import Tocify from "../components/tocify.tsx";
import hljs from "highlight.js";


const MyList = (list) => {
    const [mylist, setMylist] = useState(list.Data)
    const renderer = new marked.Renderer()
    const tocify = new Tocify()

    useEffect(() => {
        setMylist(list.Data);
    });


    renderer.heading = function (text, level, raw) {
        const anchor = tocify.add(text, level);
        return `<a id="${anchor}" href="#${anchor}" class="anchor-fix"><h${level}>${text}</h${level}></a>\n`;
    };

    marked.setOptions(
        {
            renderer: renderer,
            gfm: true,
            pedantic: false,
            sanitize: false,
            tables: true,
            breaks: false,
            smartLists: true,
            highlight: function (code) {
                return hljs.highlightAuto(code).value;
            }
        }
    );

    return (
        <div>
            <Head>
                <title>Home</title>
            </Head>
            <Header/>
            <Row className="comm-main" type="flex" justify="center">
                <Col className="comm-left" xs={24} sm={24} md={16} lg={18} xl={14}>
                    <div>
                        <List
                            header={<div>最新日志</div>}
                            itemLayout="vertical"
                            dataSource={mylist}
                            renderItem={item => (
                                <List.Item>
                                    <div className="list-title">
                                        <Link href={{pathname: '/detailed', query: {id: item.ID}}}>
                                            <a>{item.Title}</a>
                                        </Link>
                                    </div>
                                    <div className="list-icon">
                                        <span><ClockCircleTwoTone/> {item.AddTime}</span>
                                        <span><ContainerTwoTone/> {item.TypeTitle}</span>
                                        <span><FireTwoTone/> {item.ViewCount}</span>
                                    </div>
                                    <div className="list-context"
                                         dangerouslySetInnerHTML={{__html: item.Introduce}}
                                    >
                                    </div>
                                </List.Item>
                            )}
                        />
                    </div>
                </Col>

                <Col className="comm-right" xs={0} sm={0} md={7} lg={5} xl={4}>
                    <Author/>
                    <Advert/>
                </Col>
            </Row>
            <Footer/>

        </div>
    )
}

MyList.getInitialProps = async (content) => {

    let id = content.query.id;

    const promise = new Promise(resolve => {
        Axios(servicePath.getArticleTypeList + id).then(
            (res) => {
                resolve(res.data);
            }
        )
    })

    return await promise
}

export default MyList