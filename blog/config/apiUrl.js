let ipUrl = "http://localhost:8080/default/"

let servicePath = {
    getArticleList: ipUrl + 'list', //请求文章页
    getArticleById: ipUrl + 'article?id=', //请求详细页
    getTypeInfo: ipUrl + 'getType', //获得文章类别
    getArticleTypeList: ipUrl + 'getListById?id=' //获得类型Id的文章列表

}

export default servicePath
