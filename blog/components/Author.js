import {Avatar, Divider} from 'antd'
import '../static/style/components/author.css'
import {GithubOutlined, QqOutlined, WechatOutlined} from "@ant-design/icons";

const Author = () => {

    return (
        <div className="author-div comm-box">
            <div><Avatar size={100}
                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFkAAABZCAYAAABVC4ivAAAGyUlEQVR4Xu2cXWwUVRTH/7M7szOl0BZoKZTW0lqQxMiDhhiN0Rjji5oQoxiIGKP4gOInBIEgXyKgYlUiEqJgomBExTdfjF8xEn3Q+GBEwUqhlAKlqC2U7uzsx5h7t/sxpcvObnfOrOHM87333Pu7/3vuOed2qwwcrLbBn6cEFIbsKV85OEP2njFDJmDMkBkyBQECG+yTGTIBAQITrGSGTECAwAQrmSETECAwwUpmyAQECEywkhkyAQECE6xkhkxAgMAEK5khExAgMMFKZsgEBAhMsJKvZMjq5HuAWD9iAwddYQhWXgvbjiMxdNhVe8pGZank0LQl0K9aDdgWzK6NiPbuuywTdeKdMNq2Q1E0WKfeRuTkm5QM89oqO8h60yqEpj8JJTheTt6On0ekux1Wz/ZRFxMYNxsVs/ZAKDn52YgP/opI14uI9X+dFwBFg7KCrDdvgN6wBAgY6bUnIt0wjy5D7N8vR+WhVt8CvWULgpXXiT8jSbcRmxM9+xEiXZtgxwcpWOa0URaQhWqNq9uh1d4HKGoGcPgIzKPL8/pl0V9vXgttyoPpE5BW9cXfEDm+wVdV+w5ZHvfWdgSrb85SojjyvyDcsbSgi0ytuQNGy2YExl3jUDUSJqzevYh0bfRF1b5CVifdBaPlJQSM1sxRs+OI9X8D869nkLBOFXzM5alo2QKt7gGH25G+2idV+wbZaNkKrf4h5/G2LUTPfgLz2KoxK06MrTcuR8CY4dgo6at7P4R5bHXBG1hsB3LIwQlzYczYiGDVTSMuqkFYPTsQ6X652LVc0i8QapChnVpzu8PXS1Wf/xHm8fWIX/ipZPZyDUQKWW98FqGGp6Bokx3zSZjHETmxFdG+j3MuWPrumbsQqGjLXIxDhxA+/Ehet6I3rUCoYSkUtcapaqsXke5XYZ3Z4yloEsjiQtKb1yE4fo7zQhKKGvgB4c7leS+4ilnvQKu7H0AgDSR+8RDCfy7O21d0yBXqiYQn2ncA4Y4nPAPtOWStfhGMGZuhqNUjfOMgor17XflGEZ7pDUtHXGTJxCPW/x3Chxe58uHJUPENaLXzACWUTF1iAzLqsM689/+FLGYujrlWNx9QghJMYugIzGNrXMWuoamPQm9en9kkOybSQCCgD4fCcUT7PkW4Y4lrSMJ16I3LoAQrERFpeNcm132Laei5ksWkhIIqZu9DsOpGWYdwG68mAa+Fok50AE1YPU5lD8fBZucK1wyE+whUzoF1aqfrPsU2JIEsJidueqhVrvynaC/djFCwVje8NqdrcJ4OIJVwFAK6WGiF9iODXMjEklW4lRkFAxh5ySVPxwfJ8CxVs7BjiJ77TKbiftcrstdbdpD15hcgIKeqcGKyuaKIlBtSa25zpuQuI5ZCNn4sbcsGsrz523ZAE8X6dJFIpMIiTHssp5sZVdHSe+SPvccCrpC+ZQF59BjWfZFIbtDMndAm3T0cwQwj8LkwlNoI3yEnaxiiRFmVEYcsEn2F8JHFBflWo/UVhOofvqQwJFV98nUZl/vx+QY5ZxYo1fc+zM6VRfEITV0Mvel5KKF6Z387nqzCndyG2N+fFzV2sZ3IIYsCkX7VGqiifjycdaUmL0qbsoYxRsXlKkJJO+KxNdyBaN9+WKd3F3RSyh6yhDv9aYhHz+znpdTCYwPfyxJnKV+bcxWGUrBE2TN+4Wf5TCWyRq8+z5Usnvb1aY8jWDX3EuVKYUX7PH1hlpvbtAJq9a2j1D4yWGUU8/v8vBW9YjbCM8iiWF4xazeCE24YUXlLJXBxCPVGzx0QVZpi5l5QH0WrhVa3EEHxNJX1jpj0IEPydds6vaugMd029gyymIDM3JrXQQmOy5pP8sk+YXZCq73X7TxL0k5U7Kwz7444WTZi/3yBoT8WlMTGaIN4ClkYzNSBFVl9E1UvcbFVtL0ln58oPwF56NA8aVI8AojyaWD89Yh0b/U04vAcslxM43OI9X+L6Nn9aaZ+Q6bcXM8h51qMA7JtyT+tipzYUtK1a1MWwGh9LV0HyVZySQ3lGYwhE9BmyAx5bATYXWRHF+yTx6Ymvvh8/JdlHF14I17HqAyZIZeUQHmEcEggYXbJQk0pPyWgIWC0AIomh72yk5FSkr3MWAyZAPQVDpndhSca4+jCE6zOQRkyAWSjdRu0KQuTlmwLVo/4JWl7SS2Ln6yJX0Nh+IeX8YGDnr6A5Jq8byFcSWmW+WAMmWCDGDJDJiBAYIKVzJAJCBCYYCUzZAICBCZYyQyZgACBCVYyQyYgQGCClcyQCQgQmGAlM2QCAgQmWMkMmYAAgQlWMkMmIEBggpXMkAkIEJj4DwDnB2FVrQgKAAAAAElFTkSuQmCC"/>
            </div>
            <div className="author-introduction">
                打工人，打工魂，打工方为人上人
                <Divider>社交账号</Divider>
                <a href={"https://gitee.com/timedb"} target={"_blank"}>
                    <Avatar size={28} icon={<GithubOutlined/>} className="account"/>
                </a>

                <a href={"http://qq.qqdna.com/qqhao/1658002533.html"} target={"_blank"} title={""}>
                    <Avatar size={28} icon={<QqOutlined/>} className="account"/>
                </a>
                

            </div>
        </div>
    )

}

export default Author