let ipUrl = "http://127.0.0.1:8080/admin/"

let servicePath = {
    checkLogin: ipUrl + 'login',//检测用户名密码
    getTypeInfo: ipUrl + 'getType', //获取文章类别
    addArticle: ipUrl + 'addArticle', //添加文章
    updateArticle: ipUrl + 'updateArticle',//跟新文章
    getArticleList: ipUrl + "articleList",//获取全部文章
    deleteArticle: ipUrl + "deleteArticle",//删除文章
    getArticleById : ipUrl + "getArticleById",
}

export default servicePath