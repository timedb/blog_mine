import React, {useState, useEffect} from "react"
import {List, Row, Col, Modal, message, Button} from "antd";
import axios from "axios";
import servicePath from "../config/apiUrl";
import "../static/css/ArticList.css"

const {confirm} = Modal

function ArticleList(props) {

    const [list, setList] = useState([])

    const getList = () => {
        let token = localStorage.getItem("openId");
        let user = localStorage.getItem("user");
        axios({
            method: 'get',
            url: servicePath.getArticleList,
            params: {
                "token": token,
                "user": user
            }
        }).then(
            res => {
                setList(res.data.Data);
            }
        )
    }

    const delArticle = (id) => {
        confirm({
            title: "警告，这篇文章会永远的离开你",
            content: "如果点击了OK，文章会永远的离开你",
            onOk() {
                let token = localStorage.getItem("openId");
                let user = localStorage.getItem("user");
                let dataPros = new FormData;
                dataPros.append("articleId", id)
                axios({
                    method: "post",
                    url: servicePath.deleteArticle,
                    params: {
                        "token": token,
                        "user": user
                    },
                    data: dataPros
                }).then(
                    res => {
                        message.success("文章删除成功");
                        getList();
                    }
                ).catch(err => {
                    if (err.response.status === 401) {
                        message.error("认证错误");
                        localStorage.clear();
                    } else {
                        message.error("保存失败");
                    }
                })
            },
            onCancel() {
                message.success("明智的选择，召唤师");
            }

        })
    }

    //修改文章的方法
    const updateArticle = (id) => {
        props.history.push('/index/add/' + id)
    }

    useEffect(() => {
        getList();
    }, [])


    return (
        <div>
            <List
                header={
                    <Row className="list-div">
                        <Col span={8}>
                            <b>标题</b>
                        </Col>
                        <Col span={3}>
                            <b>类别</b>
                        </Col>
                        <Col span={4}>
                            <b>发布时间</b>
                        </Col>
                        <Col span={3}>
                            <b>浏览量</b>
                        </Col>

                        <Col span={4}>
                            <b>操作</b>
                        </Col>
                    </Row>

                }
                bordered
                dataSource={list}
                renderItem={item => (
                    <List.Item>
                        <Row className="list-div">
                            <Col span={8}>
                                {item.Title}
                            </Col>
                            <Col span={3}>
                                {item.TypeTitle}
                            </Col>
                            <Col span={4}>
                                {item.AddTime}
                            </Col>
                            <Col span={3}>
                                {item.ViewCount}
                            </Col>

                            <Col span={4}>
                                <Button type="primary" onClick={function () {  //跳转bug
                                    updateArticle(item.ID)
                                }}>修改</Button>&nbsp;

                                <Button danger type="primary" onClick={function () {
                                    delArticle(item.ID);
                                }}>删除 </Button>
                            </Col>
                        </Row>

                    </List.Item>
                )}
            />

        </div>
    )

}

export default ArticleList